# webull-desktop PKGBUILD

I wanted to try this software and the existing PKGBUILD on [AUR](https://aur.archlinux.org/packages/webull-desktop) was both ancient and non-functional.

I built this as a one-off over a couple of hours trying to force some things I ended up unable to clean/change in the existing PKGBUILD, **BUT**, it does run, and it is the current available version as of 2024-01-02!

## Problems (and work-arounds)
1. I cannot force this to link against up-to-date qt5 libraries (in particular `libQt5WebEngine*.so*`) due to compiled, bundled libraries linked against the included older qt5 libraries.
2. Because of (1), I cannot force this to run in QT wayland (only QT xcb, so it will need Xwayland if you're in a wayland environment)
3. The download works right now, but:
  - The suffix on the downloaded file right before `.deb` changes periodically
  - So does the sha256sum
  - Because of this, you may find that you need to:
    - Manually download the current `.deb` from [Webull](https://www.webull.com/introduce/desktop-native)
	- Modify the `source_x86_64` variable
	- Modify the `sha256sums_x86_64` variable to match the output of `sha256sum [DOWNLOADED_FILENAME]`
